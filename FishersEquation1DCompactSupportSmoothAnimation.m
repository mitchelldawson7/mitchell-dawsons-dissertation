% this m file gives an animation of an approximation
% the 1d Fisher's equation with a 1 - |x| compactly supported
% initial condition using a forward Euler finite difference method.

h=1/20                        % define dx as 1/n: n positive integer
k=1/100                        % define dt
kappa = 0.1                     %define kappa
L = 40               % define half length of domain
T = 30           %define total time 


stability_coefficient = kappa*k/h^2 % stability if <1/2

%----------------------------------------
% defining the right hand side matrix for the forward euler time step
r=2*L/h+1;

B = [ones(r,1) -2*ones(r,1) ones(r,1)];
RHS = spdiags(B, [-1 0 1], r, r); %add in full(spdiags(...)) for debug
RHS(1,2) = 2;
RHS(r,r-1) = 2;


%------------------------------------------
x = (-L:h:L)';                %define x as 1/h coloumn vector of 

alpha = 10;
U = (ones(length(x),1) - (x/alpha).^2).^2;    % define U(x,0)

   %define compact support
CSedge = -alpha;               
CSedgeNode = (CSedge + L)/h + 1; % define using -L + h(CSedgeNode - 1) = CSedge
for j = 1:CSedgeNode
    U(j) = 0;
    U(r+1-j)=0;
end
    

for s = k:k:T             

f = U.*(1-U);      % iterate 1/k times
U = U + kappa*k/h^2*RHS*U + k*f ;


if mod(s,10*k)==0
plot(x,U,'r');             % plot approximation
xlabel('x');
ylabel('u/U','Rotation',0)
title('Exact & Finite step Forward Euler solution Fisher''s equation')

hold on;

axis([-L L -0.1 1.1]);
legend(['time =', num2str(sprintf('%.2f',s),3)]); %show time step
 

hold off;
drawnow
end
end 

%-------------------------------------------



