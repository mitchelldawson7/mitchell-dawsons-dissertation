% this m file gives an animation of an approximation
% the 1d Heat equation with a cosine-wave initial condition
%  and neumann boundary conditions on [0,L]
% using a forward Euler finite difference method.



h=1/20                        % define dx as 1/n: n positive integer
k=1/100                        % define dt
kappa = 0.1                     %define kappa    
T = 1
L = 1
stability_coefficient = kappa*k/h^2 % stability if <1/2

%----------------------------------------
% defining the right hand side matrix for the forward euler time step
r=L/h+1;

B = [ones(r,1) -2*ones(r,1) ones(r,1)];
RHS = spdiags(B, [-1 0 1], r, r);

RHS(1,2) = 2;      %this sorts the neumann bconds
RHS(r,r-1) = 2;

%------------------------------------------
x = (0:h:L)';                %define x as 1/h coloumn vector of 
U = cos(2*pi*x/L);                % define U(x,0)

for s = k:k:T                   % iterate 1/k times
U = U + kappa*k/h^2*RHS*U;

plot(x,U,'r');                    % plot approximation
xlabel('x');
ylabel('u/U','Rotation',0)
title('Exact & Finite step Forward Euler solution of the heat equation')

hold on;

plot(x,exp(-4*kappa*sym(pi)^2*s)*cos(2*pi*x/L),'b');    %plot exact

axis([0 L -1 1]);
legend('Approximation','Exact');
 

hold off;
drawnow
end 

%-------------------------------------------



