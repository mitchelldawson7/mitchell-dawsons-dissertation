% this m-file produces an error plot of for the 
% 2d heat equation on a square with neumann boundary conditions
% uses finite difference method forward Euler 

ErrorVector = [];           %define empty error vector
hstep = [];

for p = 3:9       % try h = 1/8, 1/16, ...,1/256
h=1/(2^p);                         % cycle h over p values 
kappa = 0.1
k = 1/4*h^2/kappa;        % time step dt
L = 1         % half length and width of domain

stabilityCoefficient = kappa*k/(h^2) % stable if less than 1/8

%------------------------------------------------
r = L/h+1;

% -----create the RHS matrix ----------------------------

A1 = [ones(r,1) -2*ones(r,1) ones(r,1)];
A = spdiags(A1,[-1 0 1], r, r);
A(1,2)=2;
A(r,r-1) = 2;

I = speye(r); 
RHS = kron(I,A) + kron(A,I);
%----------------------------------------------------------

[X,Y] = meshgrid(0:h:L,0:h:L); %gives meshgrid where (i,j) <=> row j/h + 1, 
                               % column L/h + 1

%--boundary conditions----------------------------------------------------                             

m=1
n=1

U = cos(m*pi*X/L).*cos(n*pi*Y/L); %define U(x,0)

%--get U in correct form for iteration-----------------------

Ucol = (reshape(U',1,r^2))';

%----iterate once and reshape ----------------------------

Ucol = Ucol + kappa*k/h^2*RHS*Ucol; % iterate

U = (reshape(Ucol,r,r))';   % reshape

%--- find exact at time k ---------------------------------
u = cos(m*pi*X/L).*cos(n*pi*Y/L)*exp(-(pi)^2*kappa*(m^2+n^2)/(L^2)*k);

%----error analysis----------------------------------------------------

E = u - U ;   %compute tau
Emax = max(max(abs(E)));    %compute infinity norm of tau

ErrorVector = [ErrorVector,Emax]; %add new row to errorvector of 
                                        % value of infinity norm of tau
                                        
hstep = [hstep h];   %define vector of values of h which cycled over

end

%------------------------------------------


% Create figure
figure1 = figure('Name','Local Truncation Error in Heat Equation for varying h, with k = O(h^2)');

% Create axes
axes1 = axes('Parent',figure1);
box(axes1,'on');
hold(axes1,'all');

% Create plot
plot(log(hstep),log(ErrorVector),'Parent',axes1,'DisplayName','data1');

% Create xlabel
xlabel('log(h) = log(x seperation) = log(dx)');

% Create ylabel
ylabel('log(Error)','Rotation',0);

% Create title
title('Local Truncation Error in forward Euler finite difference approximation of heat equation for various values of h, k = O(h\^2)');

