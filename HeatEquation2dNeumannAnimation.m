% this m-file produces an animation of the evolution of the 
% 2d heat equation on a square with neumann boundary conditions
% uses finite difference method forward Euler 


h = 1/10       %define dx = dy = node width
k = 1/40       % time step dt
T = 10          % end time
L = 2         % half length and width of domain

kappa = 0.1

stabilityCoefficient = kappa*k/(h^2) % stable if less than 1/8

r = L/h+1;

% -----create the RHS matrix ----------------------------

A1 = [ones(r,1) -2*ones(r,1) ones(r,1)];
A = spdiags(A1,[-1 0 1], r, r);
A(1,2)=2;
A(r,r-1) = 2;

I = speye(r); 
RHS = kron(I,A) + kron(A,I);
%----------------------------------------------------------

[X,Y] = meshgrid(0:h:L,0:h:L); %gives meshgrid where (i,j) <=> row j/h + 1, 
                               % column L/h + 1

%--boundary conditions----------------------------------------------------                             

m=1
n=1

U = cos(m*pi*X/L).*cos(n*pi*Y/L);



Umax=max(max(U));
Umin=min(min(U));

surf(X,Y,U)


axis([0 L 0 L Umin Umax]);
colorbar
caxis([Umin Umax])

%--get U in correct form for iteration-----------------------

Ucol = (reshape(U',1,r^2))';

%----iterate and plot ----------------------------

for timeloop = k:k:T
Ucol = Ucol + kappa*k/h^2*RHS*Ucol; % iterate

U = (reshape(Ucol,r,r))';   % reshape

%-- uncomment below two lines ------------
%--- to compare against actual solution -------------------

%u = cos(m*pi*X/L).*sin(n*pi*Y/L)*exp(-(pi)^2*kappa*(m^2+n^2)/(L^2)*timeloop);

%surf(X,Y,u) 

%----------------- PLOTTING INSTRUCTIONS-----------------
if mod(timeloop,10*k) < k/10

hold off;
colorbar('delete')
surf(X,Y,U) 
set(gcf, 'renderer', 'zbuffer');
axis([0 L 0 L Umin Umax]);
colorbar;
caxis([Umin Umax]);
legend(['time =', num2str(sprintf('%.2f',timeloop),3)]); %show time step
view(90,270); %uncomment to view from above

drawnow
end

end


