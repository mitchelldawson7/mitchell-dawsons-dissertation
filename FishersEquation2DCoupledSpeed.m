% this m-file produces an animation of the evolution u and v of the 
% 2d Fisher's equation on the fast diffusion route boundary.
% uses finite difference method forward Euler 


dy = 1/10     % define node width (typical 1/10) 
dx = dy       %define node width same in both directions
k = 1/40       % time step dt (typical 1/20)
T = 160          % end time (typical 140)
H = 8         % height of domain (typical 10)
L = 40         % half length of domain (typical 20)
d = 0.01 % (typical 0.01) diffusion coeff on plane V
D = 0.1 % (typical 0.05) diffusion coeff on line U
Mu = 5  % (typical 5) control line to region - controls spread angle 
Nu = 0.1  % (typical 1) controls region to line

%-----------------------------------------------------

stabilityCoefficient = d*k/dx^2 % not sure what this should be?

xNodes = L/dx+1; %total number x nodes
yNodes = H/dy+1; %total number y nodes

% -----create the RHS matrix for 2d----------------------------

A2 = [ones(xNodes,1) -2*ones(xNodes,1) ones(xNodes,1)]; %change 2 to 4 and uncomment below
A = spdiags(A2,[-1 0 1], xNodes, xNodes);
A(1,2)=2;
A(xNodes,xNodes-1) = 2;

B = spdiags(A2,[-1 0 1], yNodes, yNodes);
B(1,2)=2;
B(yNodes,yNodes-1) = 2;

I1 = speye(yNodes);  
I2 = speye(xNodes);
RHS2 = kron(I1,A) + kron(B,I2); % + 4*speye(yNodes*xNodes);
%----create RHS matrix for 1d ------------------------------------------------------

A1 = [ones(xNodes,1) -2*ones(xNodes,1) ones(xNodes,1)];
RHS1 = spdiags(A1, [-1 0 1], xNodes, xNodes); %add in full(spdiags(...)) for debug
RHS1(1,2) = 2;
RHS1(xNodes,xNodes-1) = 2;

%----set up 1d and 2d grids-------------------

[X,Y] = meshgrid(0:dx:L, 0:dy:H); %gives meshgrid where (i,j) <=> rows H/dy + 1, 
                                                    % column 2*L/h + 1
x = (0:dx:L)';           
                                                   
%--boundary conditions----------------------------------------------------                              

V = (1-((X-L/8).^2 + (Y-H/4).^2)).^2; %initial condition

for i = 1:yNodes          %this takes care of the compact support
    for j = 1:xNodes
        if (X(i,j)-L/8)^2 + (Y(i,j)-H/4)^2 > 1 
            V(i,j)=0;
        end
    end
end

U = zeros(length(x),1);

centre = [L/8,H/4];

%--- set up ghost points -------------

boundary = Y == 0;
firstInterior = Y == dy;

%--first plot-----------------------------------------------

%--get V in correct form for iteration-----------------------

Vcol = (reshape(V',1,xNodes*yNodes))';

%----iterate and plot ----------------------------
Vnew = V;
Unew = U;
j=2;
% --- sort out speedtrap------------------
Tstart = k
Tend = 140       
Rmax = zeros(round((Tend - Tstart)/k),1);

I = logical(Vnew(Y==centre(2))>=0.5); %find all places where wave is halfway through

for timeloop = k:k:T
    
% identify Uold,Vold with previous timestep
Uold = Unew;    
Vold = Vnew;
    
% get Unew (this timestep U) from Uold (last timestep U) (U correct on line) 

Unew = Uold + D*k/(dx)^2*(RHS1*Uold) + k*Nu*Vold(boundary) - k*Mu*Uold; % update U on line

% turn old V into coloumn Voldcol

Voldcol = (reshape(Vold',1,xNodes*yNodes))'; %turn Vold into Voldcol

% iterate V (V correct on plane)

Vnewcol = Voldcol + d*k/(dx)^2*(RHS2*Voldcol) + k*Voldcol.*(1-Voldcol); % iterate Vcol (correct on boundary and plane)

%turn Vcol into V

Vnew = (reshape(Vnewcol, xNodes, yNodes))' ;

% get vectors of solution
Ux = [0; Uold; 0];
Vx = [0 ; Vold(boundary) ; 0];
Vxp = [Vold(boundary);0;0];
Vxm = [0;0;Vold(boundary)];
Vfi = [0;Vold(firstInterior);0];

% apply boundary conditions on interior x nodes (V correct on Plane &interior x)

VxNew = Vx + d*k/(dx)^2*(2*Vfi + Vxp + Vxm - 4*Vx + 2*dy/d*(Mu*Ux - Nu*Vx)) + k*Vx.*(1-Vx);

% apply boundary conditions on end x nodes (V correct everywhere)

VxNew(2) = Vx(2) +  d*k/(dx)^2*(2*Vfi(2) + 2*Vx(3) - 4*Vx(2) + 2*dy/d*(Mu*Ux(2) - Nu*Vx(2))) + k*Vx(2)*(1-Vx(2));

VxNew(xNodes+1) = Vx(xNodes+1) +  d*k/(dx)^2*(2*Vfi(xNodes+1) + 2*Vx(xNodes) - 4*Vx(xNodes+1) + 2*dy/d*(Mu*Ux(xNodes+1) - Nu*Vx(xNodes+1))) + k*Vx(xNodes+1)*(1-Vx(xNodes+1));

% remove 0's from end of vector

VxNew = VxNew(2:xNodes+1);

Vnew(boundary) = VxNew;

if (timeloop >= Tstart && timeloop <= Tend)
    
I = logical(Vnew(Y==centre(2))>=0.5); %find all places where wave is halfway through

% find x position of first and last on line
Lfar = find(I,1,'first')*dx; 
Rfar = find(I,1,'last')*dx;
% find distance between these and 
furthest = sqrt(max((centre(1) - Lfar)^2,(centre(1) - Rfar)^2));
% take max distance

Rmax(round(timeloop/k))=furthest;  %position farthest largest radius and add to radius vector

end

end

%--------plot time stuff ----------
Time = Tstart:k:Tend;
plot(Time,Rmax)
xlabel('t');
ylabel('Rmax','Rotation',0);
title('Radius at time t');

slm = slmengine(Time,Rmax,'degree',1,'knots',[0.025,50,140],'plot','on')
%}
