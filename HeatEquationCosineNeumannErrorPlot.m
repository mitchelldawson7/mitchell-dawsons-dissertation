% this m file produces a plot of the infinity norm of 
% the error vector tau vs the value of h with which it is assosiated 
% h is cycled from 1/8 to 1/256 and 
% the stability coeffiecient is held fixed at 1/4
% by defining k in terms of h and kappa

ErrorVector = [];           %define empty error vector
hstep = [];

for p = 3:9       % try h = 1/8, 1/16, ...,1/256
h=1/(2^p);                         % cycle h over p values 
kappa = 1;                     % define kappa    
k=1/4*h^2/kappa;        % define dt
L=1;

%----------------------------------------------
r = L/h + 1;

% defining the right hand side matrix for the forward euler time step
B = [ones(r,1) -2*ones(r,1) ones(r,1)];
RHS = spdiags(B, [-1 0 1], r, r);

RHS(1,2) = 2;   %sorts boundary conds
RHS(r,r-1) = 2;

%------------------------------------------

x = (0:h:L)';               %define x as L/h+1 coloumn vector

U = cos(2*pi*x/L);                % define U(x,0)
U = U + kappa*k/h^2*RHS*U;        % iterate once
u = exp(-4*kappa*pi^2*k)*cos(2*pi*x/L); % find exact at time k

tau = u - U ;   %compute tau
tauInfNorm = max(abs(tau));    %compute infinity norm of tau

ErrorVector = [ErrorVector,tauInfNorm]; %add new row to errorvector of 
                                        % value of infinity norm of tau
                                        
hstep = [hstep h];   %define vector of values of h which cycled over
                                        
end

%------------------------------------------


% Create figure
figure1 = figure('Name','Local Truncation Error in Heat Equation for varying h, with k = O(h^2)');

% Create axes
axes1 = axes('Parent',figure1);
box(axes1,'on');
hold(axes1,'all');

% Create plot
plot(log(hstep),log(ErrorVector),'Parent',axes1,'DisplayName','data1');

% Create xlabel
xlabel('log(h) = log(x seperation) = log(dx)');

% Create ylabel
ylabel('log(Error)','Rotation',0);

% Create title
title('Local Truncation Error in forward Euler finite difference approximation of heat equation for various values of h, k = O(h\^2)');
