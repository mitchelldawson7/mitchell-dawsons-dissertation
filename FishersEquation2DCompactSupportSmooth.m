% this m-file produces an animation of the evolution of the 
% 2d Fisher's equation on a square with neumann boundary conditions
% uses finite difference method forward Euler 


h = 1/4       %define dx = dy = node width 1/4
k = 1/20       % time step dt 1/20
T = 55          % end time
L = 30         % half length and width of domain

kappa = 0.2 %0.2

stabilityCoefficient = kappa*k/(h^2) % stable if less than 1/8

r = 2*L/h+1;

% -----create the RHS matrix ----------------------------

A1 = [ones(r,1) -4*ones(r,1) ones(r,1)];
A = spdiags(A1,[-1 0 1], r, r);
A(1,2)=2;
A(r,r-1) = 2;

I = speye(r); 
RHS = kron(I,A) + kron(A,I) + 4*kron(I,I);
%----------------------------------------------------------

[X,Y] = meshgrid(-L:h:L,-L:h:L); %gives meshgrid where (i,j) <=> row j/h + 1, 
                               % column L/h + 1

%--boundary conditions----------------------------------------------------
[V,W] = meshgrid(-L:h:L, -L:h:L);                                

U = (ones(r,r)-(V.^2 + W.^2)).^2;

for i = 1:r          %this takes care of the compact support
    for j = 1:r
        if V(i,j)^2 + W(i,j)^2 > 1 
            U(i,j)=0;
        end
    end
end

Umax=max(max(U));
Umin=min(min(U));

surf(X,Y,U)
set(gcf, 'renderer', 'zbuffer');
axis([-L L -L L Umin Umax]);
colorbar;
%legend(['time = 0']); %show time step
view(90,270);
caxis([Umin Umax])
axis('equal')
M(1) = getframe;


%--get U in correct form for iteration-----------------------

Ucol = (reshape(U',1,r^2))';

%----iterate and plot ----------------------------
j=2;

for timeloop = k:k:T
Ucol = Ucol + kappa*k/h^2*RHS*Ucol + k*Ucol.*(1-Ucol); % iterate

U = (reshape(Ucol,r,r))';   % reshape

if mod(timeloop,10*k) == 0; 

hold off;    
colorbar('delete')
surf(X,Y,U)                  % plot approximation

set(gcf, 'renderer', 'zbuffer');
axis([-L L -L L Umin Umax]);
axis('equal')
colorbar;
caxis([Umin Umax]);
legend(['time =', num2str(sprintf('%.2f',timeloop),3)]); %show time step
view(90,270); %comment to view from side

drawnow
M(j) = getframe;
j=j+1;
end
end


