% this m-file produces an animation of the evolution of the 
% 2d Fisher's equation on a rectangle with neumann boundary conditions on
% outside of the rectangle plus an order 1 approximation of a fast
% diffusion route on the boundary. the population V does not satisfy the
% fishers equation of the boundary and so the solution becomes
% discontinuous.
% uses finite difference method forward Euler 


dy = 1/10     % define node width (typical 1/10) 
dx = dy       %define node width same in both directions
k = 1/20       % time step dt (typical 1/20)
T = 140          % end time (typical 140)
H = 10         % height of domain (typical 10)
L = 20         % half length of domain (typical 9)
d = 0.01 % (typical 0.01) diffusion coeff on plane V
D = 0.05 % (typical 0.05) diffusion coeff on line U
Mu = 10  % (typical 5) control line to region - controls spread angle 
Nu = 1  % (typical 1) controls region to line

%-----------------------------------------------------

stabilityCoefficient = d*k/dx^2 % not sure what should be?

xNodes = 2*L/dx+1; %total number x nodes
yNodes = H/dy+1; %total number y nodes

% -----create the RHS matrix for 2d----------------------------

A2 = [ones(xNodes,1) -4*ones(xNodes,1) ones(xNodes,1)];
A = spdiags(A2,[-1 0 1], xNodes, xNodes);
A(1,2)=2;
A(xNodes,xNodes-1) = 2;

B = spdiags(A2,[-1 0 1], yNodes, yNodes);
B(1,2)=2;
B(yNodes,yNodes-1) = 2;

I1 = speye(yNodes);  
I2 = speye(xNodes);
RHS2 = kron(I1,A) + kron(B,I2) + 4*speye(yNodes*xNodes);
%----create RHS matrix for 1d ------------------------------------------------------

A1 = [ones(xNodes,1) -2*ones(xNodes,1) ones(xNodes,1)];
RHS1 = spdiags(A1, [-1 0 1], xNodes, xNodes); %add in full(spdiags(...)) for debug
RHS1(1,2) = 2;
RHS1(xNodes,xNodes-1) = 2;

%----set up 1d and 2d grids-------------------

[X,Y] = meshgrid(-L:dx:L, 0:dy:H); %gives meshgrid where (i,j) <=> rows H/dy + 1, 
                                                    % column 2*L/h + 1
x = (-L:dx:L)';           
                                                   
%--boundary conditions----------------------------------------------------                              

V = (1-((X+3*L/4).^2 + (Y-H/4).^2)).^2; %initial condition

for i = 1:yNodes          %this takes care of the compact support
    for j = 1:xNodes
        if (X(i,j)+3*L/4)^2 + (Y(i,j)-H/4)^2 > 1 
            V(i,j)=0;
        end
    end
end

U = zeros(length(x),1);

%--first plot-----------------------------------------------
Vmax=max(max(V));
Vmin=min(min(V));

surf(X,Y,V)
set(gcf, 'renderer', 'zbuffer');
axis([-L L 0 H Vmin Vmax]);
colorbar;
legend(['time = 0']); %show time step
%view(90,270);
caxis([Vmin Vmax])
axis('equal')
view(0,90); %comment to view from side
M(1) = getframe;

%--get V in correct form for iteration-----------------------

Vcol = (reshape(V',1,xNodes*yNodes))';

%----iterate and plot ----------------------------
j=2;

for timeloop = k:k:T
Vcol = Vcol + d*k/(dx)^2*RHS2*Vcol + k*Vcol.*(1-Vcol); % iterate

Vx = Vcol(1:length(x));
U = U + D*k/(dx)^2*RHS1*U + k*Nu*Vx - k*Mu*U;

% apply boundary condition on x=0
Vcol(1:length(x)) = (d*Vcol((1:length(x))+length(x)) + dx*Mu*U(1:length(x)))/(d + Nu*dx);    

%plotting instructions
if mod(timeloop,10*k) == 0; 
    
TotalPopulationCol = Vcol + [U;zeros(length(Vcol)-length(U),1)];    
    
%V = (reshape(Vcol,xNodes,yNodes))';   % reshape

TotalPopulation = (reshape(TotalPopulationCol,xNodes,yNodes))';

hold off;    
colorbar('delete')
surf(X,Y,TotalPopulation)                  % plot approximation

set(gcf, 'renderer', 'zbuffer');
axis([-L L 0 H Vmin Vmax]);
axis('equal')
colorbar;
caxis([min(min(min(V)),0) max(max(max(V)),1)]);
legend(['time =', num2str(sprintf('%.2f',timeloop),3)]); %show time step
view(0,90); %comment to view from side

drawnow
M(j) = getframe;
j=j+1;
end
end

%}
